import { Helmet } from 'react-helmet-async';

import SEDetail from "../Components/SE-Detail";

import { motion, AnimatePresence } from 'framer-motion';

import { useLang } from '../Components/LclProvider';

export default function SE() {
    const [lang] = useLang();

    return <AnimatePresence exitBeforeEnter>
        <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1, transition:{ delay: 0.5 } }}
        exit={{ opacity: 0 }}>
                <Helmet>
                    <title>{ lang === 'en' ? 'Snake and Ladders | Portfolio' : 'Serpent Échelle | Portfolio' }</title>
                    <meta name='description' content={ lang === 'en' ? 'Snake and Ladders is one of the projects accomplished with basic web development languages ​​and pure logistics!' : 'Serpent Échelle est un des projets accomplis avec les langues de base de développement web et de la pure logistique!' }/>
                </Helmet>

                <SEDetail/>
        </motion.div>
    </AnimatePresence>
}