import { Helmet } from 'react-helmet-async';

import EPDetail from "../Components/EP-Detail";

import { motion, AnimatePresence } from 'framer-motion';

import { useLang } from '../Components/LclProvider';

//Exporte funtion principale du document 
export default function EP() {
    const [lang] = useLang();

    return <AnimatePresence exitBeforeEnter>
        <motion.div 
        initial={{ opacity: 0 }}
        transition={{ delay:0.4 }}
        animate={{ opacity: 1, transition:{ delay: 0.5 } }}
        exit={{ opacity: 0 }}>
            <Helmet>
                <title>{ lang === 'en' ? 'Lost in Space | Portfolio' : 'Espace Perdu | Portfolio' }</title>
                <meta name='description' content={ lang === 'en' ? "Lost in Space is one of the projects accomplished with the C# language and other API tools!" : "Espace Perdu est un des projets accomplis avec la langue C# et d'autres outils API!" }/>
            </Helmet>

            <EPDetail/>
        </motion.div>
    </AnimatePresence>
}