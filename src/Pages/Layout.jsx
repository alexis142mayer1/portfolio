import { Suspense } from "react";
import { Outlet } from "react-router-dom";

import Footer from "../Components/Footer";
import Header from "../Components/Header";
import Loading from '../Components/Loading';

//Exporte funtion principale du document 
export default function Layout(){
    return <>
        <Header/>
        {/*Intentionnellement rien ajouter dans le loading page, gerer par les transitions framer-motion*/}
        <Suspense fallback={<Loading/>}>
            <Outlet/>
        </Suspense>
        
        <Footer/>
    </>
}