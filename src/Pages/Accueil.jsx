import { Helmet } from 'react-helmet-async';

import AccueilAPropos from '../Components/Accueil-APropos';
import AccueilIntroduction from '../Components/Accueil-Introduction';
import AccueilProjet from '../Components/Accueil-Projet';

import { motion, AnimatePresence } from 'framer-motion';

import { useLang } from '../Components/LclProvider';

//Exporte funtion principale du document 
export default function Accueil(){
    const [lang] = useLang();

    return <AnimatePresence exitBeforeEnter>
        <motion.div
        initial={{ opacity: 0 }}
        transition={{ delay:0.5 }}
        animate={{ opacity: 1 }}
        exit={{ opacity: 0 }}>
                <Helmet>
                    <title>{ lang === 'en' ? 'Home | Portfolio' : 'Accueil | Portfolio' }</title>
                    <meta name='description' content={ lang === 'en' ? 'Discover who I am and my talents in reflections of my accomplished projects!' : 'Découvrez qui je suis et mes talents en reflets de mes projets accomplis!' }/>
                </Helmet>

                <AccueilIntroduction/>

                <AccueilAPropos/>

                <AccueilProjet/>
        </motion.div>
    </AnimatePresence>
}