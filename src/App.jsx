import './App.css';
import { Route, Routes } from 'react-router-dom';
//import { lazy } from 'react';

import { LclProvider } from './Components/LclProvider';
import Layout from './Pages/Layout';

import Accueil from './Pages/Accueil';
import SE from './Pages/SE';
import EP from './Pages/EP';

//const Accueil = lazy(() => import ('./Pages/Accueil'));
//const SE = lazy(() => import ('./Pages/SE'));
//const EP = lazy(() => import ('./Pages/EP'));

//Exporte funtion principale de l'application
export default function App() {

    return <LclProvider>
        <Routes>
            <Route path='/' element={<Layout/>}>
                <Route index element={<Accueil/>}/>
                <Route path='SE' element={<SE/>}/>
                <Route path='EP' element={<EP/>}/>
            </Route>
        </Routes>
    </LclProvider>
}