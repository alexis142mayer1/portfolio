const themes = {
    light: {
        bgGrad: 'linear-gradient(to right bottom, #c9d6df, #bad0e1, #acc9e4, #9fc2e6, #94bbe9)',
        bg: '#C9D6DF',
        txt: '#000',
        txtTitre: '#35858B'
    },
    dark: {
        bgGrad: 'linear-gradient(to right bottom, #254b62, #386277, #4b798c, #6192a2, #77abb7)',
        bg: '#254B62',
        txt: '#fff',
        txtTitre: '#FFFEB7'
    }
}

let themeActif = 'dark';

const changerTheme = (theme) => {
    themeActif = theme;

    let root = document.documentElement.style;

    for(let variable in themes[theme]){
        
        if(themes[theme].hasOwnProperty(variable)){
            root.setProperty('--' + variable, themes[theme][variable]);
        }
    }
}

changerTheme(themeActif);

export { themeActif, changerTheme }