import styles from './Accueil-APropos.module.css';

import { FormattedMessage } from 'react-intl';

import AOS from 'aos';
import 'aos/dist/aos.css';
AOS.init();

//Exporte funtion principale du document et voici la documentation pour AOS:
//https://github.com/michalsnik/aos
export default function AccueilAPropos() {
    return <div className={styles.main} id='APropos'>
        <div className={styles.wrapper}>
            <div>
                <h1 className={styles.txtTitreAPropos}><FormattedMessage id='app.apropos.titre'/></h1>
            </div>
            <p className={styles.txtAPropos}><FormattedMessage id='app.apropos.contenu'/></p>
            <p className={styles.txtAPropos}><FormattedMessage id='app.apropos.info'/></p>
        </div>

        <div className={styles.imgHolderLangues}>
            <div data-aos="fade-up" data-aos-duration="1000">
                <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/javascript/javascript-plain.svg" alt='Javascript' className={styles.imgLangue}/>
                <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/git/git-plain-wordmark.svg" alt='Git' className={styles.imgLangue}/>
                <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/android/android-plain.svg" alt='Android' className={styles.imgLangue}/>
                <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/apple/apple-original.svg" alt='Apple' className={styles.imgLangue}/>
                <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/nodejs/nodejs-plain-wordmark.svg" alt='Node Js' className={styles.imgLangue}/>
                <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/cd/Visual_Studio_2017_Logo.svg/1024px-Visual_Studio_2017_Logo.svg.png" alt='VS' className={styles.imgLangue}/>
            </div>
            
            <div data-aos="fade-up" data-aos-duration="1500">
                <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/npm/npm-original-wordmark.svg" alt='Npm' className={styles.imgLangue}/>
                <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/php/php-plain.svg" alt='PHP' className={styles.imgLangue}/>
                <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/react/react-original-wordmark.svg" alt='React' className={styles.imgLangue}/>
                <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/vscode/vscode-original.svg" alt='VS Code' className={styles.imgLangue}/>
                <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/c/c-line.svg" alt='C' className={styles.imgLangue}/>
            </div>

            <div data-aos="fade-up" data-aos-duration="2000">
                <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/html5/html5-plain-wordmark.svg" alt='HTML' className={styles.imgLangue}/>
                <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/java/java-plain-wordmark.svg" alt='Java' className={styles.imgLangue}/>
                <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/mysql/mysql-plain-wordmark.svg" alt='MySQL' className={styles.imgLangue}/>
                <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/csharp/csharp-line.svg" alt='C Sharp' className={styles.imgLangue}/>
            </div>
            
            <div data-aos="fade-up" data-aos-duration="2500">
                <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/cplusplus/cplusplus-line.svg" alt='C Plus Plus' className={styles.imgLangue}/>
            </div>
        </div>
</div>
}