import styles from './Footer.module.css';

//import ContactForm from './ContactForm';

import imgIconEmail from '../Resources/imgIconEmail.WebP';
import imgIconDownload from '../Resources/imgIconDownload.WebP';
//import imgFacebook from '../Resources/imgIconFacebook.WebP';
import imgIconUpDark from '../Resources/imgIconUpDark.WebP';
import pdfResume from '../Resources/Resume.pdf'

import { motion, AnimatePresence } from 'framer-motion';

import { FormattedMessage } from 'react-intl';

//Exporte function principale du document
export default function Footer(){
    return <AnimatePresence exitBeforeEnter>
    <motion.footer className={styles.footer} id='Contact'
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ opacity: 0 }}>
        <div className={styles.wrapper}>
            <div className={styles.containerContact}>
                <div className={styles.containerMedia}>
                    <div className={styles.container}>
                        <a href={pdfResume} download='CV_EN_AlexisMayer.pdf' className={styles.txtLink}><FormattedMessage id='app.footer.telechargerResumer'/><img src={imgIconDownload} alt='Icon Download' className={styles.imgLink}/></a>
                        <a href='mailto:alexis142mayer@gmail.com' target='_blank' rel="noreferrer" className={styles.txtLink}><FormattedMessage id='app.footer.envoyerCourriel'/><img src={imgIconEmail} alt='Icon Email' className={styles.imgLink}/></a>
                    </div>
                    {/*
                    Removed because not enough social media

                    <ul className={styles.container}>
                        <li><a href='https://www.facebook.com/alexis.mayer.750' target="_blank" rel="noreferrer"><img src={imgFacebook} alt='Logo Facebook' className={styles.imgMedia}/></a></li>
                    </ul>
                    */}
                </div>
                
                {/* 
                Removed contact form because not completely functional

                <div className={styles.containerForm}>
                    <ContactForm/>
                </div>
                */}

                <div className={styles.containerLegal}>
                    <p>&copy; Alexis Mayer</p>
                    <p><FormattedMessage id='app.footer.legal'/></p>
                </div>

            </div>
        </div>

        <a href='#Header'><img src={imgIconUpDark} alt='Icon Up Dark' className={styles.imgIconUp}/></a>
    </motion.footer>
    </AnimatePresence>
}