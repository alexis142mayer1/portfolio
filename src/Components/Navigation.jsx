import styles from './Navigation.module.css';
import { useNavigate, useLocation } from 'react-router-dom';

import { AnimatePresence, motion } from 'framer-motion';

import { FormattedMessage } from 'react-intl';

//Exporte function principale du document
export default function Navigation() {
    const navigate = useNavigate();
    const location = useLocation();

    const navAccueil = () => {
        if(location.pathname !== "/"){
            navigate("/")
        }
    }

    const easing = [0.6, -0.05, 0.01, 0.99];

    return <>
        <AnimatePresence exitBeforeEnter>
            <motion.nav 
            className={styles.container}
            initial={{ opacity: 0, y: -100 }}
            animate={{ opacity: 1, y: 0 }}
            transition={{ ease:easing, duration: .6, delay: .8 }}
            exit={{ opacity: 0 }}>
                <ul className={styles.listeNav}>
                    <li>
                        <a onClick={() => {navAccueil()}} href="#Header" className={styles.txtLinkNav}><FormattedMessage id='app.nav.accueil'/></a>
                    </li>
                    <li>
                        <a onClick={() => {navAccueil()}} href="#APropos" className={styles.txtLinkNav}><FormattedMessage id='app.nav.apropos'/></a>
                    </li>
                    <li>
                        <a onClick={() => {navAccueil()}} href="#Projet" className={styles.txtLinkNav}><FormattedMessage id='app.projet.titre'/></a>
                    </li>
                    <li>
                        <a onClick={() => {navAccueil()}} href="#Contact" className={styles.txtLinkNav}>Contact</a>
                    </li>
                </ul>
            </motion.nav>
        </AnimatePresence>
    </>
}