import styles from './ContactForm.module.css';
import { useState } from 'react';

import emailValidator from 'email-validator';

import { useLang } from './LclProvider';

import { FormattedMessage } from 'react-intl';

export default function ContactForm() {
    const [lang] = useLang();

    const [courrielTouched, setCourrielTouched] = useState(false);

    const [nom, setNom] = useState("");
    const changerNom = (event) => setNom(event.currentTarget.value);

    const [courriel, setCourriel] = useState("");
    const changerCourriel = (event) => {
        setCourriel(event.currentTarget.value);
        setCourrielTouched(true);
    }

    const [message, setMessage] = useState("");
    const changerMessage = (event) => setMessage(event.currentTarget.value);

    const [agreement, setAgreement] = useState(false);
    const changerAgreement = (event) => setAgreement(event.currentTarget.checked);

    //Function utiliser pour imprimer courriel envoyer et reinitialiser input.
    const handleSubmit = (event) => {
        event.preventDefault();

        if(courriel !== "" && emailValidator.validate(courriel) && message !== "" && nom !== "" ) {

            if(agreement){
                console.log(`Contenu du courriel envoyer ->\n\nNom: ${nom}\nCourriel: ${courriel}\nMessage: ${message}`);
        
                setNom("");
                setCourriel("");
                setMessage("");
                setAgreement(false);
                setCourrielTouched(false);
            } else{
                if(lang === 'en') {
                    alert("To send a message, you must confirm sharing your information by checking the condition.");
                } else {
                    alert("Pour envoyer un message, vous devez confirmer votre partage d'information en cochant la condition.");
                }
            }
        } else{
            if(lang === 'en') {
                alert("To send a message, you must fill out every field.");
            } else {
                alert("Pour envoyer un message, vous devez remplir chacun champ.");
            }        
        }
    }

    return <form className={styles.frmContact} onSubmit={handleSubmit}>
        <div className={styles.containerField}>
            <div>
                <label>
                    <FormattedMessage id='app.contact.nom'/>
                    <input type="text" className={styles.txtField} value={nom} onChange={changerNom}/>
                </label>
            </div>

            <label>
                <FormattedMessage id='app.contact.courriel'/>
                <input type="email" className={styles.txtField} value={courriel} onChange={changerCourriel}/>

                {courrielTouched && !emailValidator.validate(courriel) && <div className={styles.txtErreur}><FormattedMessage id='app.contact.courrielInvalide'/></div>}
            </label>
        </div>
        <div className={styles.containerField}>
            <label>
                <FormattedMessage id='app.contact.message'/>
                <input type="textarea" className={styles.txtMessage} value={message} onChange={changerMessage}/>
            </label>
        </div>

        <div className={styles.containerField}>

            <label className={styles.containerField}>
                <input type="checkbox" checked={agreement} onChange={changerAgreement} className={styles.chkConfirmer}></input>
                
                <div className={styles.txtConfirme}><FormattedMessage id='app.contact.confirme'/></div>

                <input type="submit" value='&#10095;' className={styles.btnSubmit}/>
            </label>
        </div>
    </form>
}