import styles from './Accueil-Introduction.module.css';

import imgFace from '../Resources/imgFace.WebP';
import imgWaving from '../Resources/imgWaving.WebP';
import {ReactComponent as Shape}  from '../Resources/svgShape.svg';

import { FormattedMessage } from 'react-intl';

//Exporte function principale du document et pour le code svg, j'ai copier l'exercise suivant pour la forme dynamique:
//https://webkul.com/blog/morphing-using-svg-animate-css/
export default function AccueilIntroduction() {
    return <div className={styles.main}>
        <div>
            <h2  className={styles.txtH2}>
                <div className={styles.txtSalut}><FormattedMessage id='app.intro'/> <span className={styles.wave}><img src={imgWaving} alt="Waving emoji" className={styles.imgWaving}/></span></div>
                <div className={styles.txtMoi}><FormattedMessage id='app.intro.moi'/><span>Alexis Mayer</span></div>
                <div className={styles.txtSalut}><FormattedMessage id='app.intro.emploi'/></div>
            </h2>
        </div>

        <div className={styles.container}>
            <div className={styles.imgHolderProfile}>
                <div className={styles.holderProfile}>
                    <img src={imgFace} alt='Profile' className={styles.imgProfile}/>
                </div>
            </div>

            <div className="wrapper">
                <Shape className={styles.svgShape}/>
            </div>
        </div>
    </div>
}