import styles from './Accueil-Projet.module.css';
import { useNavigate } from 'react-router-dom';

import imgSE from '../Resources/imgSE.WebP';
import imgEP from '../Resources/imgEP.WebP';

import { FormattedMessage } from 'react-intl';

//Exporte function principale du document 
export default function AccueilProjet() {
    const navigate = useNavigate();
    
    return <div className={styles.main} id='Projet'>
        <div className={styles.container}>
            <h1 className={styles.txtTitreProjet}><FormattedMessage id='app.projet.titre'/></h1>
            <div><FormattedMessage id='app.projet.soustitre'/></div>
        </div>
        
        <div className={styles.wrapper}>
            <div className={styles.carte}>
                <a onClick={() => {navigate("/SE")}} href='#Header' className={styles.txtLinkProjet}>
                    <h1 className={styles.txtTitreCarteProjet}><FormattedMessage id='app.projet.SE'/></h1>
                    <img src={imgSE} alt='Background SE' className={styles.imgProjet}/>
                </a>
            </div>
    
            <div className={styles.carte}>
                <a onClick={() => {navigate("/EP")}} href="#Header" className={styles.txtLinkProjet}>
                    <h1 className={styles.txtTitreCarteProjet}><FormattedMessage id='app.projet.EP'/></h1>
                    <img src={imgEP} alt='Background EP' className={styles.imgProjet}/>
                </a>
            </div>
        </div>
    </div>
}
