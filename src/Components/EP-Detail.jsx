import styles from './EP-Detail.module.css';

import AliceCarousel from 'react-alice-carousel';
import "react-alice-carousel/lib/alice-carousel.css";

import imgGameplay from '../Resources/imgEP-gameplay.WebP';
import imgSprite from '../Resources/imgEP-sprite.WebP';

import docxProposition from '../Resources/PropositionJeux.docx';
import zipProjet from '../Resources/ProjetJeux.zip';

import { FormattedMessage } from 'react-intl';

//Exporte function principale du document et voici l'information GitHub pour la gallerie:
//https://www.npmjs.com/package/react-simple-image-slider
export default function EPDetail() {
    return <div className={styles.main}>
        <div className={styles.wrapper}>
            <h1 className={styles.txtTitreProjet}><FormattedMessage id='app.projet.EP'/></h1>
            <div className={styles.txtHolderLinks}>
                <a href={docxProposition} download='PropositionJeux_AlexisMayer.docx' className={styles.txtLink}><FormattedMessage id='app.EP.proposition'/></a>
                <a href={zipProjet} download='ProjetJeux_AlexisMayer.zip' className={styles.txtLink}><FormattedMessage id='app.projet.titre'/></a>
            </div>
            <p>
                <FormattedMessage id='app.EP.contenu'/>
            </p>
        </div>

        <div className={styles.container}>
            <AliceCarousel autoPlay autoPlayInterval="3000" disableButtonsControls={true} infinite={true} disableDotsControls={true}>
                <img src={imgGameplay} className={styles.imgSlider} alt='Gameplay Espace Perdu'/>
                <img src={imgSprite} className={styles.imgSlider} alt='Sprite Espace Perdu'/>
            </AliceCarousel>
        </div>
    </div>
}