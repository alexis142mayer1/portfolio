import styles from './SE-Detail.module.css';

import AliceCarousel from 'react-alice-carousel';
import "react-alice-carousel/lib/alice-carousel.css";

import imgAccueil from '../Resources/imgSE-accueil.WebP'
import imgJouer from '../Resources/imgSE-jouer.WebP'
import imgPlatforme from '../Resources/imgSE-platforme.WebP'

import { FormattedMessage } from 'react-intl';

//Exporte function principale du document et voici l'information GitHub pour la gallerie:
//https://www.npmjs.com/package/react-alice-carousel
export default function SEDetail() {
    return <div className={styles.main}>
        <div className={styles.wrapper}>
            <h1 className={styles.txtTitreProjet}><FormattedMessage id='app.projet.SE'/></h1>
            <a href='https://alexis142mayer1.gitlab.io/serpent-echelle/' target="_blank" rel="noreferrer" className={styles.txtLink}><FormattedMessage id='app.SE.cliquez'/></a>
            <p>
                <FormattedMessage id='app.SE.contenu'/>
            </p>
        </div>

        <div className={styles.container}>
            <AliceCarousel autoPlay autoPlayInterval="3000" disableButtonsControls={true} infinite={true}  disableDotsControls={true}>
                <img src={imgAccueil} className={styles.imgSlider} alt='Accueil Serpent Echelle'/>
                <img src={imgPlatforme} className={styles.imgSlider} alt='Platforme Serpent Echelle'/>
                <img src={imgJouer} className={styles.imgSlider} alt='Partie jouer Serpent Echelle'/>
            </AliceCarousel>
        </div>
    </div>
}