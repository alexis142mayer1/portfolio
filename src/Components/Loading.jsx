import styles from './Loading.module.css';

import { motion, AnimatePresence } from 'framer-motion';

//Exporte function principale du document
export default function Header() {
    return <AnimatePresence exitBeforeEnter>
        <motion.div className={styles.main}>
        </motion.div>
    </AnimatePresence>
}