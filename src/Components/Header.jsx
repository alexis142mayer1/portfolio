import styles from './Header.module.css';
import { Link } from 'react-router-dom';

import { useState } from 'react';

import Navigation from './Navigation';

import { themeActif, changerTheme  } from '../Theme';

import { motion, AnimatePresence } from 'framer-motion';

import { useLang } from './LclProvider';

import imgThemeDark from '../Resources/imgThemeDark.WebP';
import imgThemeLight from '../Resources/imgThemeLight.WebP';

//Exporte function principale du document
export default function Header() {
    const [lang, setLang] = useLang();
    const [lclTheme, setTheme] = useState(themeActif);

    const handleChangeTheme = () => {
        if (themeActif === 'light'){
            changerTheme('dark');
        } else {
            changerTheme('light');
        }
        setTheme(themeActif);
    }

    const handleChangeLang = () => {
        if (lang === 'en'){
            setLang('fr');
        } else {
            setLang('en');
        }
    }

    return <AnimatePresence exitBeforeEnter>
        <header className={styles.main} id='Header'>
            <div className={styles.container}>
                <Link to="/" className={styles.txtLinkHeader}>
                    <motion.h1
                    initial={{ opacity: 0 }}
                    transition={{ delay:0.4 }}
                    animate={{ opacity: 1 }}
                    exit={{ opacity: 0 }}>Alexis . M()
                    </motion.h1>
                </Link>

                <motion.div className={styles.wrapper}
                        initial={{ opacity: 0 }}
                        animate={{ opacity: 1 }}
                        exit={{ opacity: 0 }}>
                    <a onClick={handleChangeLang} href="#Header" className={styles.btnLang}>{lang === 'en' ? ' FR ' : ' EN '}</a>
                    
                    <a onClick={handleChangeTheme} href="#Header" className={styles.txtLinkTheme}>{ lclTheme === 'light' ? <img src={imgThemeDark} alt='Theme dark' className={styles.imgTheme}/> : <img src={imgThemeLight} alt='Theme light' className={styles.imgTheme}/> }</a>
                </motion.div>
            </div>
            
            <Navigation/>
        </header>
    </AnimatePresence>
}