import { createContext, useContext, useState } from "react";
import { IntlProvider } from "react-intl";
import FrancaisData from '../i18n/fr.json';
import AnglaisData from '../i18n/en.json';

const translations = {
    en: AnglaisData,
    fr: FrancaisData
}

const LclContext = createContext();

export function LclProvider(props) {
    const [lang, setLang] = useState('en');

    return <LclContext.Provider value={[lang, setLang]}>
        <IntlProvider locale={lang} messages={translations[lang]}>
            {props.children}
        </IntlProvider>
    </LclContext.Provider>
}

export function useLang(){
    const [lang, setLang] = useContext(LclContext);
    
    return [lang, setLang];
}